%{
	#include "parser.h"
	#include "parser.tab.h"
	std::ofstream outFile;
	std::ofstream attrFile;
	std::ostringstream sm;
	int lineNo;
	std::stack<int> currStack;
	std::stack <pairVal> tempStack;
	std::vector<string> tempV;
	list<int> sizeList;
	list<int> symList;
	int sizeCounter = 0;
	int symCounter = 0;
	int counter = 0;
	int yylex(void);
	void yyerror(char *);
	void genParseTree(vector<string> v){
		struct pairVal tempVar;
		for (int i=0 ; i!=v.size(); i++){
			if (!(std::find_if(v[i].begin(), v[i].end(), ::islower) == v[i].end()) ){
				tempVar.val = currStack.top();
				tempVar.str = v[i];
				currStack.pop();
				tempStack.push(tempVar);
			}
			else{
				tempVar.val = counter++;
				tempVar.str = v[i];
				tempStack.push(tempVar);
			}
		}
		ostringstream toPush;
		for(    ; !tempStack.empty(); tempStack.pop()){
			toPush << counter << " -> { " << tempStack.top().val << "[label=\"" << tempStack.top().str << "\"] }\n";
		}
		currStack.push(counter++);
		outFile << toPush.str();
	}
	void append(string* array, int size){
		tempV.erase(tempV.begin(), tempV.end());
		for (int i=0;i<size;i++){
			tempV.insert(tempV.begin(), array[i]);
		}
		genParseTree(tempV);
	}

%}
%union {
	containerStruct container;
}

%token<container>  MATRIX DOT VARSYMBOL SIZE VARSIZE CHECK IDENTITY ADD SUM FIND DISPLAY INVERSE MENTIONED GIVEN MATRICES INTERGER BOOLEAN SQAURE INPUT SIZES INT FLOAT
%type<container>problem sentences sentence initMatrix withSize input typeMatrix listVarSize listVarSymbol matrices findStatement typeIdentity typeSum typeInverse given alternateDecs varSizeList
%start problem

%%
problem:
	sentences	{
		string temp[] = {"sentences"};
		append(temp, 1);
	}
	;
sentences:
	sentence DOT sentences	{
		string temp[] = {"sentence", "DOT", "sentences"};
		append(temp, 3);
	}
	|
	sentence DOT	{
		string temp[] = {"sentence", "DOT"};
		append(temp, 2);
	}
	;
sentence:
	initMatrix	{
		string temp[] = {"initMatrix"};
		append(temp, 1);
	}
	|
	findStatement 	{
		string temp[] = {"findStatement"};
		append(temp, 1);
	}
	;
initMatrix:
	SIZE typeMatrix matrices listVarSymbol listVarSize	{
		;
	}
	|
	input matrixList{
		string temp[] = {"input", "matrixList"};
		append(temp, 2);
	}
	;
matrixList:
	typeMatrix matrices varSizeList matrixList {
		string temp[] = {"typeMatrix", "matrices", "varSizeList", "matrixList"};
		append(temp, 4);
	}
	|
	typeMatrix matrices varSizeList {
		string temp[] = {"typeMatrix", "matrices", "varSizeList"};
		append(temp, 3);
	}
	;
varSizeList:
	withSize 	{
		if($1.hasSize){
			sm.str(string());
			for(auto i=sizeList.begin();i!=sizeList.end();i++){
				sm << "(size_" << *i << ")";
			}
			$$.strVal = strdup(sm.str().c_str());
			sizeList.erase(sizeList.begin(), sizeList.end());
			cout << $$.strVal <<endl;
		}
		string temp[] = {"withSize"};
		append(temp, 1);
	}
	|
	VARSYMBOL withSize{
		sm.str(string());
		if($2.hasSize && sizeList.size()==1){
			sm << "(sym_" << symCounter++ << ", size_" << sizeList.front() << ")";
			$$.strVal = strdup(sm.str().c_str());
			sizeList.erase(sizeList.begin(), sizeList.end());
			cout << $$.strVal <<endl;
		}
		else if(!$2.hasSize){
			sm << "(" << symCounter++ <<")";
		}
		else{
			cout << "Some error has occurred in varSizeList rule 2\n";
			exit(1);
		}
	}
	|
	VARSYMBOL listVarSymbol withSize 	{
		if($2.isEmpty){
			sm.str(string());
			if($3.hasSize && sizeList.size()==1){
				sm << "(sym_" << symCounter++ << ", size_" << sizeList.front() << ")";
				sizeList.erase(sizeList.begin(), sizeList.end());
				$$.strVal = strdup(sm.str().c_str());
				cout << $$.strVal <<endl;
			}
			else if(!$3.hasSize){
				sm << "(sym_" << symCounter++ << ")";
				$$.strVal = strdup(sm.str().c_str());
				cout << $$.strVal <<endl;
			}
			else{
				cout << "Some error has occurred in varSizeList rule 3_1\n";
				exit(1);
			}
		}
		else{
			sm.str(string());
			if($3.hasSize && sizeList.size()==symList.size()+1){
				sm << "(sym_" << symCounter++ << ", size_" << sizeList.front() << ")";
				sizeList.pop_front();
				auto j=symList.begin();
				for (auto i=sizeList.begin(); i!=sizeList.end(); i++,j++){
					sm << "(sym_" << *j << ", size_" << *i << ")";
				}
				$$.strVal = strdup(sm.str().c_str());
				symList.erase(symList.begin(), symList.end());
				sizeList.erase(sizeList.begin(), sizeList.end());
				cout << $$.strVal <<endl;
			}
			else if(!$3.hasSize){
				sm << "(sym_" << symCounter++ << ")";
				for(auto i=symList.begin();i!=symList.end();i++){
					sm << "(sym_" << *i << ")";
				}
				$$.strVal = strdup(sm.str().c_str());
				symList.erase(symList.begin(), symList.end());
				cout << $$.strVal <<endl;
			}
			else{
				cout << "Some error has occurred in varSizeList rule 3_2\n";
				exit(1);
			}
		}
	}
	|
	VARSYMBOL SIZE VARSIZE alternateDecs {
		$$.hasSize = true;
		sizeList.push_front(sizeCounter++);
		symList.push_front(symCounter++);
		if($4.hasSize){
			sm.str(string());
			auto j=symList.begin();
			for (auto i=sizeList.begin(); i!=sizeList.end(); i++,j++){
				sm << "(sym_" << *j << ", size_" << *i << ")";
			}
			$$.strVal = strdup(sm.str().c_str());
			symList.erase(symList.begin(), symList.end());
			sizeList.erase(sizeList.begin(), sizeList.end());
			cout << $$.strVal <<endl;
		}
		string temp[] = {"VARSYMBOL", "SIZE", "VARSIZE", "alternateDecs"};
		append(temp, 4);
	}
	;
alternateDecs:
	VARSYMBOL SIZE VARSIZE alternateDecs{
		$$.hasSize = true;
		sizeList.push_front(sizeCounter++);
		symList.push_front(symCounter++);
		string temp[] = {"VARSYMBOL", "SIZE", "VARSIZE", "alternateDecs"};
		append(temp, 4);
	}
	|
	VARSYMBOL SIZE VARSIZE{
		$$.hasSize = true;
		sizeList.push_front(sizeCounter++);
		symList.push_front(symCounter++);
		string temp[] = {"VARSYMBOL", "SIZE", "VARSIZE"};
		append(temp, 3);
	}
	;
withSize:
	SIZE VARSIZE 	{
		string temp[] = {"SIZE", "VARSIZE"};
		append(temp, 2);
		$$.hasSize = true;
		sizeList.push_front(sizeCounter++);
	}
	|
	SIZES listVarSize	{
		$$.hasSize = true;
	}
	|
	/*epsilon*/	{
		$$.hasSize = false;
		string temp[] = {"EPSILON"};
		append(temp, 1);
	}
	;
input:
	GIVEN
	|
	INPUT 	{
		string temp[] = {"INPUT"};
		append(temp, 1);
	}
	;
typeMatrix:
	/*epsilon*/	{
		string temp[] = {"EPSILON"};
		append(temp, 1);
	}
	|
	INTERGER
	|
	BOOLEAN
	|
	SQAURE	{
		string temp[] = {"SQAURE"};
		append(temp, 1);
	}
	;
listVarSize:
	VARSIZE listVarSize 	{
		sizeList.push_front(sizeCounter++);
	}
	|
	/*epsilon*/ {;}
	;
listVarSymbol:
	VARSYMBOL listVarSymbol{
		$$.isEmpty = false;
		symList.push_front(symCounter++);
	}
	|
	VARSYMBOL	{
		$$.isEmpty = false;
		symList.push_front(symCounter++);
	}
	;
matrices:
	MATRIX 	{
		string temp[] = {"MATRIX"};
		append(temp, 1);
	}
	|
	MATRICES 	{
		string temp[] = {"MATRICES"};
		append(temp, 1);
	}
findStatement:
	typeInverse
	|
	typeSum 	{
		string temp[] = {"typeSum"};
		append(temp, 1);
	}
	|
	typeIdentity
	;
typeIdentity:
	CHECK IDENTITY
	;
typeSum:
	ADD VARSYMBOL VARSYMBOL
	|
	SUM VARSYMBOL VARSYMBOL
	|
	FIND SUM 	{
		string temp[] = {"FIND", "SUM"};
		append(temp, 2);
	}
	|
	DISPLAY SUM matrices
	|
	DISPLAY SUM
	;
typeInverse:
	FIND INVERSE MATRIX
	|
	FIND INVERSE given MATRIX
	|
	FIND INVERSE MATRIX given
	;
given:
	MENTIONED
	|
	GIVEN
	;
%%
void yyerror(char *s) {
	 fprintf(stderr, "%s at line:%d \n",s,lineNo);
}

int main(int argv, char *argc[]){
	lineNo = 1;
	yyin = fopen(argc[1],"r");
	if(yyin == NULL){
		printf("unable to open file %s\n",argc[1] );
		return 0;
	}
	//yydebug = 1;
	outFile.open("parseTree.dot", ios::out | ios::trunc);
	attrFile.open("attrs.dat", ios::out | ios::trunc);
	if(!outFile.is_open()){
		printf("unable to open file %s\n","parseTree.dot" );
		return 0;
	}
	outFile << "digraph {rankdir=TB;label=\"Parse Tree\";\nsplines=\"line\";ordering=\"out\"; \n";
	//yydebug = 1;
	yyparse();
	outFile << (counter-1) << "[label=\"problem\"]\n}\n";
	fclose(yyin);
	return 0;
}
