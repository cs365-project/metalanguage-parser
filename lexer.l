Letter [a-zA-Z]
DIGIT [0-9]
AN [a-zA-Z0-9_]
space [ \n\t]

%{
#include<stdio.h>
#include<string.h>
#include<math.h>
#include<stdlib.h>
#include "parser.h"
#include "parser.tab.h"
%}
%%

"matrix"  {return MATRIX;}
"size"    {return SIZE;}
"varsize"  {return VARSIZE;}
"varsymbol" {return VARSYMBOL;}
"check"	{return CHECK;}
"identity"	{return IDENTITY;}
"add"	{return ADD;}
"sum"	{return SUM;}
"find"	{return FIND;}
"display"	{return DISPLAY;}
"inverse"	{return INVERSE;}
"mentioned"	{return MENTIONED;}
"given"	{return GIVEN;}
"matrices"	{return MATRICES;}
"interger"	{return INTERGER;}
"boolean"	{return BOOLEAN;}
"square"	{return SQAURE;}
"input"	{return INPUT;}
"sizes" {return SIZES;}
"." {return DOT;}

\n {lineNo++;}
[ \t]  {;}


%%
int yywrap(){return 1;}
