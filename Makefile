SHELL := /bin/bash
.PHONY: meta
.DEFAULT_GOAL := meta
meta:
	flex lexer.l
	bison -L c -d --verbose parser.y
	g++-4.8 -std=c++11 lex.yy.c parser.tab.c -lfl -w -o parser
clean:
	rm -f *.yy.*
	rm -f *.tab.*
	rm -f *.output
	rm -f *.dot
	rm -f parser
