#include <stdio.h>
#include <iostream>
#include <stack>
#include <string>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <list>
#include <algorithm>
extern FILE *yyin;
extern int lineNo;
using namespace std;
struct pairVal{
	string str;
	int val;
};
struct containerStruct{
	int intVal;
	char* strVal;
	bool hasSize;
	bool isEmpty;
};